# Oracle client

Colection of examples on how to connect to an oracle database using different programing languages. Referenced on <https://paas.docs.cern.ch/2._Deploy_Applications/Deploy_From_Git_Repository/4-add-oracle-client-to-s2i/>

Where is the oracle client installed?: `rpm -ql oracle-instantclient19.11-basic`
Where is the tnsnames.ora file?: `rpm -ql oracle-instantclient-tnsnames.ora-1:1.4.5-1.el8.cern.noarch`

## Python 

### Local environment

Execution:

```bash
# Build the docker image
docker build -t oracle-client-python:latest python/ -f python/Dockerfile
# Run the docker image with the python example mounted on the container
docker run --rm -it -v /root/go/src/oracle-client/python:/root/python -w /root/python --entrypoint=/bin/bash oracle-client-python:latest
# --------------------------
# ---- IN THE CONTAINER ----
# --------------------------
pip install -r requirements.txt
export ORACLE_USERNAME=XXXX
export ORACLE_PASSWORD=YYYY
python app.py
# sample output
# 19.11.0.0.0
```

### OKD4

Execution:

```bash
# Create BuildConfig to build the new image
oc new-build --image-stream=python:3.8-ubi8 --name='custom-python' --strategy=docker --code=https://gitlab.cern.ch/paas-tools/paas-examples/oracle-client.git --context-dir=python/
# Create a BuildConfig of the application using the image we built previously 
oc new-build --image-stream=custom-python:latest --name='oracle-client' --code=https://gitlab.cern.ch/paas-tools/paas-examples/oracle-client.git --context-dir=python/
# Create demo Pod with the imagestream that was built
oc run -i -t demo --image=registry.paas-stg.cern.ch/custom-docker-build/oracle-client:latest --command -- /bin/bash
# --------------------------
# ---- IN THE CONTAINER ----
# --------------------------
export ORACLE_USERNAME=XXXX
export ORACLE_PASSWORD=YYYY
python app.py
# sample output
# 19.11.0.0.0
```