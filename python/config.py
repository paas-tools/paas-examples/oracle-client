import os
password = os.environ['ORACLE_PASSWORD']
username = os.environ['ORACLE_USERNAME']
# this dsn value corresponds to `cerndb1` database, so user needs to accommodate it. This value can be obtained from the tnsnames.ora file.
# this dsn value can be substitute by the `TNS_ADMIN` environment variable.
# ffi: https://cx-oracle.readthedocs.io/en/latest/user_guide/connection_handling.html#net-service-names-for-connection-strings
dsn = 'cerndb1'
# this also works
# See https://cx-oracle.readthedocs.io/en/latest/user_guide/connection_handling.html
# dsn = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=pdb-s.cern.ch)(PORT=10121))(LOAD_BALANCE=off)(ENABLE=BROKEN)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=PDB_CERNDB1.cern.ch)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC))))'
encoding = 'UTF-8'